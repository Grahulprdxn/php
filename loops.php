<?php

echo "<h1>NUMERIC ARRAY</h1>";
$bikes = array("KTM", "HERO", "HONDA", "BAJAJ", "BULLET");

for($k = 0; $k < count($bikes); $k++) {
    echo $bikes[$k] . "<br>";
}

echo "<h1>ASSOCIATIVE ARRAY</h1>";
$cars = array("Volvo" => 34, "Saab" => 23, "Honda" => 24, "Toyata" => 56);

foreach($cars as $x => $y ) {
    echo " keys is: $x and value is: $y <br>";
}

echo "<h1>MULTIDIMENTIONAL ARRAY</h1>";
$vehicles = array(
    array("Volvo", 22, 18),
    array("BMW", 15, 13),
    array("Saab", 5, 2)
);

for($i = 0; $i < count($vehicles); $i++) {
    echo "<br>" . "$i is the row <br>";
    for($j = 0; $j < count($vehicles[$i]); $j++) {
        echo "And the inner Array's $j value is: " . $vehicles[$i][$j] . "<br>";
    }
}

?>
