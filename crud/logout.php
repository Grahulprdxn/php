<?php
session_start();
session_unset();
session_destroy();

if ($_SESSION['loginid'] == "") {
    header('Location: login.php');
}