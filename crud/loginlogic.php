<?php
session_start();

$servername = "localhost";
$username = "phpmyadmin";
$password = "root";
$dbname = "myDB2";

//Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['login'])) {
    $loginuser = $_POST['loginusername'];
    $loginpwd = trim($_POST['loginpwd']);
    // $loginpwd = $_POST['loginpwd'];

    $sql = "SELECT id, encrypt_password FROM testUser4 WHERE user_name='" . $loginuser ."'";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    $showecpwd = $row['encrypt_password'];

    if (password_verify($loginpwd, $showecpwd)) {
        $loginuserid = $row['id'];
        $_SESSION['loginid'] = $loginuserid;
        header('Location: index.php');
    } else {
        $notloginerr = "User not exists, Please signup!";
        session_unset();
        session_destroy();
    }
}

mysqli_close($conn);