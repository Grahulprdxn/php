<html>
  <head>
    <title>Form Validation Task</title>
    <style>
      .required, span { color: red; }
      div { margin-bottom: 20px; }
      table {
        width: 90%;
        border-collapse: collapse;
        margin: 0 auto;
      }

      td, th {
        padding: 8px;
        border: 1px solid #dddddd;
      }

      tr:nth-child(even) { background-color: #dddddd; }

      a {
        padding: 4px 4px;
        border-radius: 2px;
        color: #fff;
        text-decoration: none;
      }

      .update { background-color: #4169E1; }
      .delete { background-color: #008542; }
    </style>
  </head>
  <body>
    <?php require("formvalidationlogic.php") ?>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
      <!-- for Names -->
      <div class="names">
        <label for="fullname">Name<span class="required">*</span> :</label>
        <input type="text" name="fullname" id="fullname" value="<?= $name; ?>">
        <span><?= $nameerr; ?></span>
      </div>
      <!-- for Email -->
      <div class="email">
        <label for="email">Email<span class="required">*</span> :</label>
        <input type="text" name="email" id="email" value="<?= $email; ?>">
        <span><?= $emailerr; ?></span>
      </div>
      <!-- for Phone no. -->
      <div class="phone">
        <label for="phone">Phone no.<span class="required">*</span> :</label>
        <input type="text" name="phone" id="phone" value="<?= $phone; ?>">
        <span><?= $phoneerr; ?></span>
      </div>
      <!-- for UserName -->
      <div class="username">
        <label for="username">User-Name<span class="required">*</span> :</label>
        <input type="text" name="usernamed" id="username" value="<?= $usernamed; ?>">
        <span><?= $usernameerr; ?></span>
      </div>
      <!-- for Profile Pic -->
      <div class="profilepic">
        <label for="profilepic">Profile-pic<span class="required">*</span> :</label>
        <input type="file" name="profilepic" id="profilepic">
        <span><?= $profileerr; ?></span>
      </div>
      <!-- for Password -->
      <div class="password">
        <label for="password">Password<span class="required">*</span> :</label>
        <input type="password" name="password" id="password">
        <span><?= $passworderr; ?></span>
      </div>
      <!-- for Confirm Password -->
      <div class="confpassword">
        <label for="confpassword">Confirm Password<span class="required">*</span> :</label>
        <input type="password" name="confpassword" id="confpassword">
        <span><?= $cnfpassworderr; ?></span>
      </div>
      <!-- Submit Button -->
      <input type="submit">
    </form>
    <a href="login.php" title="login" style="color:green">Login</a>
    <?php require("showdata.php") ?>
  </body>
</html>