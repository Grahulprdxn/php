<?php

$servername = "localhost";
$username = "phpmyadmin";
$password = "root";
$dbname = "myDB2";

//Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// show data from database table
$sql = "SELECT id, name, email, phone_no, user_name, profil_pic, password, encrypt_password FROM testUser4";
$result = mysqli_query($conn, $sql);
echo "<table><tr><th>id</th><th>name</th><th>email</th><th>phone_no</th><th>username</th><th>profile_pic</th><th>password</th><th>encrypted password</th><th></th><th></th></tr>";
$abc = mysqli_num_rows($result);

if ($abc > 0) {
    while ($row = mysqli_fetch_assoc($result)) 
    {
        echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td><td>" . $row["email"] . "</td><td>" . $row["phone_no"] . "</td><td>" . $row["user_name"] . "</td><td>" . $row["profil_pic"] . "</td><td>" . $row["password"] . "</td><td>" . $row["encrypt_password"] . "</td><td><a class='update' href='updatedata.php?id=" . $row["id"] . "'>Update</a></td><td><a class='delete' href='deletedata.php?id=" . $row["id"] . "'>Delete</a></td></tr>";
    }
}
echo "</table>";
mysqli_close($conn);
?>