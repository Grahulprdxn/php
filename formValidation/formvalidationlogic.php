<?php
$name = $email = $phone = $username = "";
$nameerr = $emailerr = $phoneerr = $usernameerr = $passworderr = $profileerr = "";
$count = 1;
$namepattern = "/^[a-zA-Z ]+$/";
$emailpattern = "/^[a-z0-9]+\@[a-z]+\.[a-z]+$/";
$phonepattern = "/^\d{10}$/";
$pswdpattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$])[A-Za-z\d@$]{8,15}$/";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // For Name
    if (empty($_POST['fullname'])) {
        $nameerr = "Please fill the name field";
        $count = 0;
    } elseif (!preg_match($namepattern,$_POST['fullname'])) {
        $nameerr = "Please use aplhabets only!";
        $count = 0;
    } else {
        $name = $_POST['fullname'];
        $count = 1;
    }

    // For Email
    if (empty($_POST['email'])) {
        $emailerr = "Please fill the email field";
        $count = 0;
    } elseif (!preg_match($emailpattern,$_POST['email'])) {
        $emailerr = "Please use correct email address only!";
        $count = 0;
    } else {
        $email = $_POST['email'];
        $count = 1;
    }

    // For Phone number
    if (empty($_POST['phone'])) {
        $phoneerr = "Please fill the phone number field";
        $count = 0;
    } elseif (!preg_match($phonepattern,$_POST['phone'])) {
        $phoneerr = "Please use correct phone number only!";
        $count = 0;
    } else {
        $phone = $_POST['phone'];
        $count = 1;
    }

    // For Username
    if (empty($_POST['username'])) {
        $usernameerr = "Please fill the username field";
        $count = 0;
    } elseif (!preg_match("/^[\w0-9]+$/",$_POST['username'])) {
        $usernameerr = "Please use correct username!";
        $count = 0;
    } else {
        $username = $_POST['username'];
        $count = 1;
    }

    // For Profile Pic
    $target_file = basename($_FILES["profilepic"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // print_r($_FILES["profilepic"]);

    if (!file_exists($_FILES['profilepic']['tmp_name']) || !is_uploaded_file($_FILES['profilepic']['tmp_name'])) {
        $profileerr = "Please upload your profile picture";
        $count = 0;
    } elseif ($_FILES['profilepic']['size'] > 200000) {  // Check file size
        $profileerr = "Sorry, your file is too large.";
        $count = 0;
    } elseif ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") { // Allow certain file formats
        $profileerr = "Sorry, only JPG, JPEG & PNG files are allowed.";
    } else {
        $count = 1;
    }

    // For Password
    if (empty($_POST['password'])) {
        $passworderr = "Please fill the password field";
        $count = 0;
    } elseif (!preg_match($pswdpattern,$_POST['password'])) {
        $passworderr = "Please use altleast one uppercase, one lowercase and one special character password! the password should be of 8 - 15 charcters long";
        $count = 0;
    } else {
        $count = 1;
    }

    if ($count === 1) {
      echo "<p style='color:green'>Success</p>";
      $name = $email = $phone = $username = "";
    }
}