<html>
  <head>
    <title>Form Validation Task</title>
    <style>
      .required, span { color: red; }
      div { margin-bottom: 20px; }
    </style>
  </head>
  <body>
    <?php require("formvalidationlogic.php") ?>
    <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
      <!-- for Names -->
      <div class="names">
        <label for="fullname">Name<span class="required">*</span> :</label>
        <input type="text" name="fullname" id="fullname" value="<?= $name; ?>">
        <span><?= $nameerr; ?></span>
      </div>
      <!-- for Email -->
      <div class="email">
        <label for="email">Email<span class="required">*</span> :</label>
        <input type="text" name="email" id="email" value="<?= $email; ?>">
        <span><?= $emailerr; ?></span>
      </div>
      <!-- for Phone no. -->
      <div class="phone">
        <label for="phone">Phone no.<span class="required">*</span> :</label>
        <input type="text" name="phone" id="phone" value="<?= $phone; ?>">
        <span><?= $phoneerr; ?></span>
      </div>
      <!-- for UserName -->
      <div class="username">
        <label for="username">User-Name<span class="required">*</span> :</label>
        <input type="text" name="username" id="username" value="<?= $username; ?>">
        <span><?= $usernameerr; ?></span>
      </div>
      <!-- for Profile Pic -->
      <div class="profilepic">
        <label for="profilepic">Profile-pic<span class="required">*</span> :</label>
        <input type="file" name="profilepic" id="profilepic">
        <span><?= $profileerr; ?></span>
      </div>
      <!-- for Password -->
      <div class="password">
        <label for="password">Password<span class="required">*</span> :</label>
        <input type="password" name="password" id="password">
        <span><?= $passworderr; ?></span>
      </div>
      <!-- Submit Button -->
      <input type="submit">
    </form>
  </body>
</html>