<?php

function get_decorated_diff($old, $new)
{
     echo $old ^ $new . "<br>";
    $from_start = strspn($old ^ $new, "\0");
    $from_end = strspn(strrev($old) ^ strrev($new), "\0");
    echo $from_start . "fs <br>"; //10
    echo $from_end . "fe<br>"; //9

    $old_end = strlen($old) - $from_end;
    $new_end = strlen($new) - $from_end;
    echo $old_end . "oe <br>";//47
    echo $new_end . "os <br>";//38

    $start = substr($new, 0, $from_start); //The quick
    $end = substr($new, $new_end); //lazy dog
    $new_diff = substr($new, $from_start, $new_end - $from_start); // white rabbit jumped over the
    $old_diff = substr($old, $from_start, $old_end - $from_start); // brown fox jumped over the stupid

    $new = "$start<ins style='background-color:#ccffcc'>$new_diff</ins>$end";
    $old = "$start<del style='background-color:#ffcccc'>$old_diff</del>$end";
    return array("old"=>$old, "new"=>$new);
}

$string_old = "The quick brown fox jumped over the lazy dog and crossed";
$string_new = "The quick white rabbit jumped over the lazy dog";
$diff = get_decorated_diff($string_old, $string_new);
echo "<table>
    <tr>
        <td>".$diff['old']."</td>
        <td>".$diff['new']."</td>
    </tr>
</table>";

?>