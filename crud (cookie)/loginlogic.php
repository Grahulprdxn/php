<?php
session_start();

$servername = "localhost";
$username = "phpmyadmin";
$password = "root";
$dbname = "myDB2";

//Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['login'])) {
    $loginuser = $_POST['loginusername'];
    $loginpwd = trim($_POST['loginpwd']);
    $remember = $_POST['remember'];

    $sql = "SELECT id, encrypt_password FROM cookieTable WHERE user_name='" . $loginuser ."'";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
    $showecpwd = $row['encrypt_password'];

    $resultnum = mysqli_num_rows($result);

    if (password_verify($loginpwd, $showecpwd) && $remember) {
        // set session
        $loginuseridsession = $row['id'];
        $_SESSION['loginid'] = $loginuseridsession;
        
        // set cookie
        $loginuserid = $row['id'];
        setcookie('userid', $loginuserid, time() + 3600, "/");
        header('Location: index.php');
    } else if (password_verify($loginpwd, $showecpwd)) {
        // set session
        $loginuseridsession = $row['id'];
        $_SESSION['loginid'] = $loginuseridsession;
        header('Location: index.php');
    } else if ($loginuser == "" || $loginpwd == "") {
        $notloginerr = "Please fill both the fields";
    } else if ($resultnum > 0 && !password_verify($loginpwd, $showecpwd)) {
        $notloginerr = "Incorrect Password";
    } else {
        $notloginerr = "User not exists, Please signup!";
    }
}
