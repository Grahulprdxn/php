<?php
$servername = "localhost";
$username = "phpmyadmin";
$password = "root";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "DROP DATABASE myDB";
if ($conn->query($sql) === TRUE) {
    echo "Database Deleted successfully";
} else {
    echo "Error deleting database: " . $conn->error;
}

$conn->close();

