<?php

$servername = "localhost";
$username = "phpmyadmin";
$password = "root";
$dbname = "myDB2";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Insert data in table
$sql = "DELETE FROM cookieTable WHERE id=" . $_GET['id'] . "";
if ($conn->query($sql) === TRUE) {
    echo "Data deleted successfully";
} else {
    echo "Error deleting data: " . $conn->error;
}

header('Location: formvalidationTask.php');

$conn->close();