-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 11, 2019 at 06:42 PM
-- Server version: 5.7.27-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-11+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myDB2`
--

-- --------------------------------------------------------

--
-- Table structure for table `cookieTable`
--

CREATE TABLE `cookieTable` (
  `id` int(3) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone_no` bigint(12) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `profil_pic` mediumblob NOT NULL,
  `password` varchar(15) NOT NULL,
  `encrypt_password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cookieTable`
--

INSERT INTO `cookieTable` (`id`, `name`, `email`, `phone_no`, `user_name`, `profil_pic`, `password`, `encrypt_password`) VALUES
(1, 'rahul', 'rahul@abc.com', 9876543210, 'rahul', 0x4172726179, 'Rahul@1234', '$2y$10$H2k3.A4ayjIhCeShrpuqDOqw2TAT22d8tYlS0OwNEYa4I4nfqeN9.'),
(2, 'Hello', 'rascd@fdfd.com', 1234567789, 'hello_bye', 0x4172726179, 'Hello@1234', '$2y$10$SJUeklda8XpKP9p4AQiD.OeKx7idYE6DoXr6yIM7aMc.z/Bt8wWMG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cookieTable`
--
ALTER TABLE `cookieTable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cookieTable`
--
ALTER TABLE `cookieTable`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
