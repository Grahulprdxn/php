<?php
$servername = "localhost";
$username = "phpmyadmin";
$password = "root";
$dbname = "myDB2";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE TABLE cookieTable (
    id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    phone_no BIGINT(12) NOT NULL,
    user_name VARCHAR(20) NOT NULL,
    profil_pic MEDIUMBLOB NOT NULL,
    password VARCHAR(15) NOT NULL,
    encrypt_password VARCHAR(255) NOT NULL
)";
if ($conn->query($sql) === TRUE) {
    echo "Table created successfully";
} else {
    echo "Error creating Table: " . $conn->error;
}

$conn->close();