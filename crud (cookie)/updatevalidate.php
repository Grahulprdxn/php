$name = $email = $phone = $username = "";
$nameerr = $emailerr = $phoneerr = $usernameerr = $profileerr = $passworderr = $confpassword = "";
$namecount = $emailcount = $phonecount = $usercount = $profilecount = $pwdcount = $confpwdcount = 1;
$tempprofilecount = 1;
$namepattern = "/^[a-zA-Z ]+$/";
$emailpattern = "/^[a-z0-9]+\@[a-z]+\.[a-z]+$/";
$phonepattern = "/^\d{10}$/";
$pswdpattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$])[A-Za-z\d@$]{8,15}$/";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $confpassword = $_POST['confpassword'];

    // For Name
    if (empty($_POST['newname'])) {
        $nameerr = "Please fill the name field";
        $namecount = 0;
    } elseif (!preg_match($namepattern,$_POST['newname'])) {
        $nameerr = "Please use aplhabets only!";
        $namecount = 0;
    } else {
        $newname = $_POST['newname'];
        $namecount = 1;
    }

    // For Email
    if (empty($_POST['newemail'])) {
        $emailerr = "Please fill the email field";
        $emailcount = 0;
    } elseif (!preg_match($emailpattern,$_POST['newemail'])) {
        $emailerr = "Please use correct email address only!";
        $emailcount = 0;
    } else {
        $newemail = $_POST['newemail'];
        $emailcount = 1;
    }

    // For Phone number
    if (empty($_POST['newphoneno'])) {
        $phoneerr = "Please fill the phone number field";
        $phonecount = 0;
    } elseif (!preg_match($phonepattern,$_POST['newphoneno'])) {
        $phoneerr = "The phone number should only contain numbers and should be of length 10!";
        $phonecount = 0;
    } else {
        $newphoneno = $_POST['newphoneno'];
        $phonecount = 1;
    }

    // For Username
    if (empty($_POST['usernamed'])) {
        $usernameerr = "Please fill the username field";
        $usercount = 0;
    } elseif (!preg_match("/^[\w0-9]+$/",$_POST['usernamed'])) {
        $usernameerr = "Please use correct username!";
        $usercount = 0;
    } else {
        $newusername = $_POST['newusername'];
        require("checkforusername.php");
    }
}