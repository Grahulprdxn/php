<?php
$name = $email = $phone = $username = "";
$nameerr = $emailerr = $phoneerr = $usernameerr = $profileerr = $passworderr = $confpassword = "";
$namecount = $emailcount = $phonecount = $usercount = $profilecount = $pwdcount = $confpwdcount = 1;
$tempprofilecount = 1;
$namepattern = "/^[a-zA-Z ]+$/";
$emailpattern = "/^[a-z0-9]+\@[a-z]+\.[a-z]+$/";
$phonepattern = "/^\d{10}$/";
$pswdpattern = "/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$])[A-Za-z\d@$]{8,15}$/";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $confpassword = $_POST['confpassword'];

    // For Name
    if (empty($_POST['fullname'])) {
        $nameerr = "Please fill the name field";
        $namecount = 0;
    } elseif (!preg_match($namepattern,$_POST['fullname'])) {
        $nameerr = "Please use aplhabets only!";
        $namecount = 0;
    } else {
        $name = $_POST['fullname'];
        $namecount = 1;
    }

    // For Email
    if (empty($_POST['email'])) {
        $emailerr = "Please fill the email field";
        $emailcount = 0;
    } elseif (!preg_match($emailpattern,$_POST['email'])) {
        $emailerr = "Please use correct email address only!";
        $emailcount = 0;
    } else {
        $email = $_POST['email'];
        $emailcount = 1;
    }

    // For Phone number
    if (empty($_POST['phone'])) {
        $phoneerr = "Please fill the phone number field";
        $phonecount = 0;
    } elseif (!preg_match($phonepattern,$_POST['phone'])) {
        $phoneerr = "The phone number should only contain numbers and should be of length 10!";
        $phonecount = 0;
    } else {
        $phone = $_POST['phone'];
        $phonecount = 1;
    }

    // For Username
    if (empty($_POST['usernamed'])) {
        $usernameerr = "Please fill the username field";
        $usercount = 0;
    } elseif (!preg_match("/^[\w0-9]+$/",$_POST['usernamed'])) {
        $usernameerr = "Please use correct username!";
        $usercount = 0;
    } else {
        $usernamed = $_POST['usernamed'];
        require("checkforusername.php");
    }

    // For Profile Pic
    $target_file = basename($_FILES["profilepic"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if (!file_exists($_FILES['profilepic']['tmp_name']) || !is_uploaded_file($_FILES['profilepic']['tmp_name'])) {
        $profileerr = "Please upload your profile picture";
        $profilecount = 0;
    } elseif ($_FILES['profilepic']['size'] > 200000) {  // Check file size
        $profileerr = "Sorry, your file is too large.";
        $profilecount = 0;
    } elseif ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") { // Allow certain file formats
        $profileerr = "Sorry, only JPG, JPEG & PNG files are allowed!";
    } else {
        $profilepic = $_FILES['profilepic'];
        $profilecount = 1;
    }

    // For Password
    if (empty($_POST['password'])) {
        $passworderr = "Please fill the password field";
        $pwdcount = 0;
    } elseif (!preg_match($pswdpattern,$_POST['password'])) {
        $passworderr = "Please use altleast one uppercase, one lowercase and one special character password! the password should be of 8 - 15 charcters long";
        $pwdcount = 0;
    } else {
        $password = $_POST['password'];
        $pwdcount = 1;
    }

    // For Confirm Password
    if (empty($_POST['confpassword'])) {
        $cnfpassworderr = "Please fill the confirm password field";
        $confpwdcount = 0;
    } elseif ($password !== $confpassword) {
        $cnfpassworderr = "Please use the same password as above";
        $confpwdcount = 0;
    } else {
        $confpwdcount = 1;
    }

    if ($namecount && $emailcount && $phonecount && $usercount && $profilecount && $pwdcount && $confpwdcount) {
      $sendname = $name;
      $sendemail = $email;
      $sendphone = $phone;
      $sendusername = $usernamed;
      $sendprofilepic = $profilepic;
      $sendpassword = $confpassword;

      $name = $email = $phone = $usernamed = "";
      require("insertdata.php");
    }
}