<?php
  function addition($arrays, $total) 
  {
    GLOBAL $inc;
    $inc++;
    $barray = array();
    for($i = 0; $i < count($arrays); $i++)
    {
      for($j = $i + 1; $j < count($arrays); $j++)
      {
        array_push($barray, $arrays[$i] + $arrays[$j]);
        // print_r($barray);
        if ($inc == 1 && $arrays[$i] + $arrays[$j] == $total) {
          array_pop($barray);
        } elseif ($inc == 2 && $arrays[$i] + $arrays[$j] == $total) {
          echo "$arrays[$i] + $arrays[$j] = ";
          $inc = 0;
        }
      }
    }
    // print_r($barray);
    return $barray;
  }

  function getClosest($total, $arr) 
  {
    GLOBAL $arrays;
    $closest = null;
    foreach ($arr as $item) 
    {
      if ($closest === null || abs($total - $closest) > abs($item - $total)) {
        $closest = $item;
      }
    }
    addition($arrays, $closest);
    return $closest;
  }

  $inc = 0;
  $total = 270;
  $arrays = array(21, 32, 46, 67, 76, 48, 97, 56);
  
  print_r(getClosest($total, addition($arrays, $total)));
?>